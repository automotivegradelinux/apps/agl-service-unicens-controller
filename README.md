# UNICENS Controller Service
This is an AGL service which provides a clear API to control UNICENS network devices.
This service requires the `agl-service-unicens` in order to communicate with the respective network devices.

# API Description
## Name
The name of the provided API is `unicens-controller`.

## Verbs
| Name                        | Description                                   | JSON Parameters                                                   |
|-----------------------------|-----------------------------------------------|-------------------------------------------------------------------|
| slimamp_master_volume_set   | Set master volume of the slim amplifer.       | *Request:* { "value": 0 }<br>Valid range for `value`: 0..100      |
| amplifier_master_volume_set | Set master volume of the Fiberdyne amplifer.  | *Request:* { "value": 0 }<br>Valid range for `value`: 0..100      |
| microphone_is_available     | Check if the microphone present in network.   | *Request:* {}<br>*Response*: { "available": true }                |
| microphone_mode_set         | Set LED mode of microphone.                   | See section *Verb microphone_mode_set*                            |
| microphone_doa_get          | Get the direction of audio.                   | *Request:* {}<br>*Response*: { "value": 45 }<br>Valid range for `value`: 0..359        |
| microphone_doa_set          | Sets the direction of audio (LED).            | *Request:* { "value": 45 }<br>Valid range for `value`: 0..359                          |
| microphone_static_color_set | Sets color of 'static' LED Mode.              | *Request:* { "red": 0, "green": 0, "blue": 255 }<br>Valid range for each color: 0..255 |


### Verb microphone_mode_set
You can set an LED signaling mode with the following parameters.

| Name          | Description                                                                           |
|---------------|---------------------------------------------------------------------------------------|
| value         | none, doa, thinking, speaking, error, waking, ending, static, cylon, rainbow, wheel           |

## Events
None.

## Supported Hardware
- Slim Amplifier (Microchip Slim Board Family)
- Fiberdyne Amplifier

# Setup for a Native Linux Environment
## Prerequisites
- Setup of [AGL App-Framework](https://docs.automotivelinux.org/docs/en/master/devguides/reference/3-installing-binder-daemon.html) from [AGL OBS](https://build.opensuse.org/project/show/isv:LinuxAutomotive)
- MOST Linux Driver

## Download and Compile the Sources
Since there are two bindings required, we will create one work space and download the repositories here.

```
$ mkdir ws_unicens
$ cd ws_unicens
$ git checkout --recurse-submodules https://git.automotivelinux.org/apps/agl-service-unicens
$ git checkout --recurse-submodules https://git.automotivelinux.org/apps/agl-service-unicens-controller
```

Now create a `build` folder and build the `agl-service-unicens`.
```
$ cd agl-service-unicens
$ mkdir build
$ cd build
$ cmake ..
$ make
$ cd ..
```

Now create a `build` folder and build the `agl-service-unicens-controller`.
```
$ cd agl-service-unicens-controller
$ mkdir build
$ cd build
$ cmake ..
$ make
$ cd ..
```

## Setup Environment Variables
In the workspace folder setup the following varables. You may set the resulting paths also in your `~/.bashrc` file.
```
$ export AGL_UCS_PATH=$PWD
$ export AFM_APP_INSTALL_DIR=$PWD/agl-service-unicens/build/package
```
## Start both Services
It is possible to start both bindings by executing the following command:
```
$ afb-daemon --name=afb-unicens \
--binding=$AGL_UCS_PATH/agl-service-unicens-controller/build/package/lib/afb-ucs2-ctrl.so \
--binding=$AGL_UCS_PATH/agl-service-unicens/build/package/lib/afb-ucs2.so \
--workdir=$AGL_UCS_PATH/agl-service-unicens/build \
--roothttp=$AGL_UCS_PATH/agl-service-unicens-controller/htdocs \
--port=1234 --no-ldpaths --tracereq=common --token= --verbose
```

## Test the Services
You may open the testing web interface on [http://localhost:1234/](http://localhost:1234/) or execute
one of the following commands.

```
$ curl http://localhost:1234/api/unicens-controller/ping
$ afb-client-demo -H localhost:1234/api?token= unicens-controller ping
$ afb-client-demo -H localhost:1234/api?token= unicens-controller slimamp_master_volume_set "{'value':25}"
$ afb-client-demo -H localhost:1234/api?token= unicens-controller amplifier_master_volume_set "{'value':25}"
$ afb-client-demo -H localhost:1234/api?token= unicens-controller microphone_mode_set "{'value' : 'doa'}"
```

# Test on AGL Target Platform
# Prerequisites
- AGL SDK must be installed

## Download and Cross-Compile the Sources
Execute the follwing commands to download the sources and create the widget.
```
$ git checkout --recurse-submodules https://git.automotivelinux.org/apps/agl-service-unicens-controller
$ cd agl-service-unicens-controller
$ mkdir build
$ cd build
$ source /opt/agl-sdk/<environment-setup-user-dependent>
$ cmake ..
$ make
$ make widget
```

## Install Widget on the Target Platform
Copy the widget to the target and install by executing the following command.
```
$ afm-util install agl-service-unicens-controller.wgt

>>> Remember the following response to find out the correct port.
>>> {
>>>   "added":"agl-service-unicens-controller@<...>"
>>> }

$ afm-util info agl-service-unicens-controller@<...> | grep http-port

>>> Remember the port for the following test commands.
```

Please reboot the system and test the binding by executing the following commands. Replace `31022` by the previously
discovered http-port.
```
afb-client-demo --human 'localhost:31022/api?token=HELLO&uuid=magic' unicens-controller ping
afb-client-demo --human 'localhost:31022/api?token=HELLO&uuid=magic' unicens-controller slimamp_master_volume_set "{'value' : 35}"
afb-client-demo --human 'localhost:31022/api?token=HELLO&uuid=magic' unicens-controller amplifier_master_volume_set "{'value' : 35}"
afb-client-demo --human 'localhost:31022/api?token=HELLO&uuid=magic' unicens-controller microphone_mode_set "{'value' : 'doa'}"
```
