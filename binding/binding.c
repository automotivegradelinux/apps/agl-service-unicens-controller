/*
 *  Copyright 2019 Microchip Technology Inc. and its subsidiaries
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
#define _GNU_SOURCE
#ifndef AFB_BINDING_VERSION
#  define AFB_BINDING_VERSION 3
#endif

#include <stdio.h>
#include <string.h>
#include <wrap-json.h>
#include <afb/afb-binding.h>
#include "wrap-unicens.h"
#include "slimamp.h"
#include "amplifier.h"
#include "microphone.h"

/* callback at load of the binding */
static int preinit(afb_api_t api) {
    AFB_API_NOTICE(afbBindingRoot, "UNICENS-CONTROLLER: PREINIT");
    return 0;
}

/* callback for starting the service */
static int init(afb_api_t api) {
    AFB_API_NOTICE(afbBindingRoot, "UNICENS-CONTROLLER: INIT");
    wrap_ucs_init(api);
    wrap_ucs_subscribe_sync();
    wrap_ucs_subscriberx_sync();
    return 0;
}

static void on_availability_cb(uint16_t node, bool available) {
    amplifier_availablility_changed(node, available);
    slimamp_availablility_changed(node, available);
    microphone_availablility_changed(node, available);
}

static void on_message_rx_cb(uint16_t node, uint16_t msg_id, uint16_t data_sz, uint8_t *data_ptr) {
    microphone_message_received(node, msg_id, data_sz, data_ptr);
}

/* callback for handling events */
static void onevent(afb_api_t api, const char *event, struct json_object *object) {
    AFB_API_NOTICE(afbBindingRoot, "UNICENS-CONTROLLER: Event: %s object: %s", event, json_object_get_string(object));
    wrap_ucs_interpret_event(event, object, &on_availability_cb);
    wrap_ucs_interpretrx_event(event, object, &on_message_rx_cb);
}

static void ping(afb_req_t request) {
    AFB_API_NOTICE(afbBindingRoot, "UNICENS-CONTROLLER: Ping successful");
    afb_req_success(request, 0, NULL);
}

static const afb_verb_t verbs[] = {
    {.verb = "ping", .session = AFB_SESSION_NONE, .callback = ping, .auth = NULL},
    {.verb = "slimamp_master_volume_set", .session = AFB_SESSION_NONE, .callback = slimamp_master_vol_set_api, .auth = NULL},
    {.verb = "amplifier_master_volume_set", .session = AFB_SESSION_NONE, .callback = amplifier_master_vol_set_api, .auth = NULL},
    {.verb = "microphone_mode_set", .session = AFB_SESSION_NONE, .callback = microphone_mode_set_api, .auth = NULL},
    {.verb = "microphone_doa_get", .session = AFB_SESSION_NONE, .callback = microphone_doa_get_api, .auth = NULL},
    {.verb = "microphone_doa_set", .session = AFB_SESSION_NONE, .callback = microphone_led_dir_set_api, .auth = NULL},
    {.verb = "microphone_static_color_set", .session = AFB_SESSION_NONE, .callback = microphone_static_ledcolor_set_api, .auth = NULL},
    {.verb = "microphone_is_available", .session = AFB_SESSION_NONE, .callback = microphone_is_available_api, .auth = NULL},
    {NULL}
};

const afb_binding_t afbBindingExport = {
    .api = "unicens-controller",
    .specification = NULL,
    .verbs = verbs,
    .preinit = preinit,
    .init = init,
    .onevent = onevent,
    .userdata = NULL,
    .provide_class = NULL,
    .require_class = NULL,
    .require_api = NULL,
    .noconcurrency = 0
};
