/*
 *  Copyright 2019 Microchip Technology Inc. and its subsidiaries
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
#define _GNU_SOURCE
#ifndef AFB_BINDING_VERSION
#  define AFB_BINDING_VERSION 3
#endif

#include <stdio.h>
#include <string.h>
#include <afb/afb-binding.h>
#include <wrap-json.h>
#include "wrap-unicens.h"

#define NODE_ID         ((uint16_t)0x270U)
#define CONTROL_MASTER  0x07U
#define MUTE_VALUE      0x03FFU
#define MUTE_VALUE_HB   0x03U
#define MUTE_VALUE_LB   0xFFU
#define TX_PAYLOAD_SZ   3U

static uint8_t _tx_payload[TX_PAYLOAD_SZ] = {CONTROL_MASTER, MUTE_VALUE_HB, MUTE_VALUE_LB};
static bool _available = false;

/*****************************************************************************
 * Local prototypes
 */
static int slimamp_master_volume_set(uint8_t volume);
static int get_payload_master_volume(uint8_t sys_vol, uint8_t *payload_ptr, uint8_t payload_sz);

/*****************************************************************************
 * API
 */

extern void slimamp_availablility_changed(uint16_t node_id, bool available) {
    if (node_id == NODE_ID) {
        _available = available;
    }
}

static int slimamp_master_volume_set(uint8_t volume) {
    if (_available == false)
        AFB_API_NOTICE(afbBindingRoot, "%s: node is not available", __func__);
        return -1;

    if (volume > 100U)
        return -2; // parameter error

    if (get_payload_master_volume(volume, _tx_payload, TX_PAYLOAD_SZ) != 0)
        return -3; // calculation error

    // fire message now
    AFB_API_DEBUG(afbBindingRoot, "%s: calculated payload: {%02X,%02X,%02X}", __func__, _tx_payload[0], _tx_payload[1], _tx_payload[2]);
    wrap_ucs_i2cwrite_sync(NODE_ID, _tx_payload, TX_PAYLOAD_SZ);

    return 0;
}

/*****************************************************************************
 * Device specific helper functions
 */

static int get_payload_master_volume(uint8_t sys_vol, uint8_t *payload_ptr, uint8_t payload_sz) {
    if (payload_sz < 3U)
        return -1;

    uint16_t tmp = (uint16_t)(0x100U + 0x2FFU - (((int32_t)0x2FFU * (int32_t)sys_vol) / (int32_t)100/*max volume*/));
    payload_ptr[0] = CONTROL_MASTER;
    payload_ptr[1] = (uint8_t)((tmp >> 8U) & (uint16_t)0xFFU); //HB:Volume
    payload_ptr[2] = (uint8_t)(tmp  & (uint16_t)0xFFU); //LB:Volume
    return 0;
}

/*****************************************************************************
 * JSON API
 */

extern void slimamp_master_vol_set_api(afb_req_t request) {
    int volume = 0;
    struct json_object* j_obj = afb_req_json(request);

    AFB_API_DEBUG(afbBindingRoot, "UNICENS-CONTROLLER: %s:%s", __func__, json_object_get_string(j_obj));

    if (wrap_json_unpack(j_obj, "{s:i}", "value", &volume) == 0) {
        AFB_API_DEBUG(afbBindingRoot, "UNICENS-CONTROLLER: decoded value=%d", volume);

        if ((volume >= 0) && (volume <= 100)) {
            slimamp_master_volume_set((uint8_t)volume);
            afb_req_success(request, NULL, NULL);
        }
        else {
            afb_req_fail(request, "invalid range of parameter 'value'", NULL);
        }
    }
    else {
        afb_req_fail(request, "missing argument 'value'", NULL);
    }
}