/*
 *  Copyright 2019 Microchip Technology Inc. and its subsidiaries
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
#pragma once

#ifndef AFB_BINDING_VERSION
#  define AFB_BINDING_VERSION 3
#endif
#include <afb/afb-binding.h>

/* notification */
extern void microphone_availablility_changed(uint16_t node_id, bool available);
extern void microphone_message_received(uint16_t node, uint16_t msg_id, uint16_t data_sz, uint8_t *data_ptr);
/* JSON API */
extern void microphone_mode_set_api(afb_req_t request);
extern void microphone_doa_get_api(afb_req_t request);
extern void microphone_led_dir_set_api(afb_req_t request);
extern void microphone_static_ledcolor_set_api(afb_req_t request);
extern void microphone_is_available_api(afb_req_t request);
