/*
 *  Copyright 2019 Microchip Technology Inc. and its subsidiaries
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
#define _GNU_SOURCE
#ifndef AFB_BINDING_VERSION
#  define AFB_BINDING_VERSION 3
#endif

#include <stdio.h>
#include <string.h>
#include <afb/afb-binding.h>
#include <wrap-json.h>
#include "wrap-unicens.h"
#include "amplifier.h"
#include <math.h>

#define NODE_ID         ((uint16_t)0x510U)
#define FUNC_VOLUME     0x100U
#define OP_SET          0x00U
#define DEFAULT_VOLUME  0xF0U
#define TX_PAYLOAD_SZ   2U

static uint8_t _tx_payload[TX_PAYLOAD_SZ] = {OP_SET, DEFAULT_VOLUME};
static bool _available = false;

/*****************************************************************************
 * Local prototypes
 */
static int get_payload_master_volume(uint8_t sys_vol, uint8_t *payload_ptr, uint8_t payload_sz);
static int amplifier_master_volume_set(uint8_t volume);

/*****************************************************************************
 * API
 */

extern void amplifier_availablility_changed(uint16_t node_id, bool available) {
    if (node_id == NODE_ID) {
        AFB_API_DEBUG(afbBindingRoot, "%s: amplifier new availability=%d", __func__, available);
        _available = available;
    }
}

extern int amplifier_master_volume_set(uint8_t volume) {
    AFB_API_NOTICE(afbBindingRoot, "amplifier_master_volume_set volume=%d", volume);
    if (_available == false) {
        AFB_API_NOTICE(afbBindingRoot, "%s: node is not available", __func__);
        return -1;
    }

    if (volume > 100U) {
        AFB_API_NOTICE(afbBindingRoot, "%s: invalid volume value %d", __func__, volume);
        return -2; // parameter error
    }

    int logvalue = abs((int)(log1p((double)volume) * (double)100 / log1p((double)100)));
    AFB_API_NOTICE(afbBindingRoot, "%s: logarithic value=%d", __func__, logvalue);

    if (get_payload_master_volume((uint8_t)(logvalue), _tx_payload, TX_PAYLOAD_SZ) != 0) {
        AFB_API_NOTICE(afbBindingRoot, "%s: cannot calculate paylaod", __func__);
        return -3; // calculation error
    }

    // fire message now
    AFB_API_DEBUG(afbBindingRoot, "%s: calculated payload: {%02X,%02X}", __func__, _tx_payload[0], _tx_payload[1]);
    wrap_ucs_sendmessage_sync(NODE_ID, FUNC_VOLUME, _tx_payload, TX_PAYLOAD_SZ);

    return 0;
}

/*****************************************************************************
 * Device specific helper functions
 */

static int get_payload_master_volume(uint8_t sys_vol, uint8_t *payload_ptr, uint8_t payload_sz) {
    if (payload_sz < 2U)
        return -1;

    uint32_t new_vol = (uint32_t)((uint32_t)sys_vol * 255) / 100;   // calculate value{0..100} to value{0..255}

    payload_ptr[0] = OP_SET;
    payload_ptr[1] = (uint8_t)new_vol;                              // use 0..255 now
    return 0;
}


/*****************************************************************************
 * JSON API
 */

extern void amplifier_master_vol_set_api(afb_req_t request) {
    int volume = 0;
    struct json_object* j_obj = afb_req_json(request);

    AFB_API_DEBUG(afbBindingRoot, "UNICENS-CONTROLLER: %s:%s", __func__, json_object_get_string(j_obj));

    if (wrap_json_unpack(j_obj, "{s:i}", "value", &volume) == 0) {
        AFB_API_DEBUG(afbBindingRoot, "UNICENS-CONTROLLER: decoded value=%d", volume);

        if ((volume >= 0) && (volume <= 100)) {
            AFB_API_DEBUG(afbBindingRoot, "amplifier_master_vol_set calling amplifier");
            int res = amplifier_master_volume_set((uint8_t)volume);
            AFB_API_DEBUG(afbBindingRoot, "amplifier returning %d", res);
            afb_req_success(request, NULL, NULL);
        }
        else {
            afb_req_fail(request, "invalid range of parameter 'value'", NULL);
        }
    }
    else {
        afb_req_fail(request, "missing argument 'value'", NULL);
    }
}
