/*
 *  Copyright 2019 Microchip Technology Inc. and its subsidiaries
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
#pragma once

#ifndef AFB_BINDING_VERSION
#  define AFB_BINDING_VERSION 3
#endif

#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <afb/afb-binding.h>

extern int wrap_ucs_init(afb_api_t apiHandle);

/* Asynchronous API: result callback */
typedef void (*wrap_ucs_availability_cb_t)(uint16_t node, bool available);
typedef void (*wrap_ucs_rx_message_cb_t)(uint16_t node, uint16_t msg_id, uint16_t data_sz, uint8_t *data_ptr);
typedef void (*wrap_ucs_result_cb_t)(uint8_t result, void *user_ptr);

/* Asynchronous API: functions */
extern int wrap_ucs_i2cwrite(uint16_t node,
        uint8_t *data_ptr,
        uint8_t data_sz,
        wrap_ucs_result_cb_t result_fptr,
        void *result_user_ptr);

/* Synchronous API: functions */
extern int wrap_ucs_subscribe_sync(void);
extern int wrap_ucs_subscriberx_sync(void);
extern int wrap_ucs_interpret_event(const char *event, struct json_object *object, wrap_ucs_availability_cb_t callback);
extern int wrap_ucs_interpretrx_event(const char *event, struct json_object *object, wrap_ucs_rx_message_cb_t callback);
extern int wrap_ucs_i2cwrite_sync(uint16_t node, uint8_t *data_ptr, uint8_t data_sz);
extern int wrap_ucs_sendmessage_sync(uint16_t src_addr, uint16_t msg_id, uint8_t *data_ptr, uint8_t data_sz);
