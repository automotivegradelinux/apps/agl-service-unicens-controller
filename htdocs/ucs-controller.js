/*
 *  Copyright 2019 Microchip Technology Inc. and its subsidiaries
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

    var afb = new AFB("api", "mysecret");
    var ws;
    var evtidx=0;
    var numid=0;

    function syntaxHighlight(json) {
        if (typeof json !== 'string') {
             json = JSON.stringify(json, undefined, 2);
        }
        json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
        return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
            var cls = 'number';
            if (/^"/.test(match)) {
                if (/:$/.test(match)) {
                    cls = 'key';
                } else {
                    cls = 'string';
                }
            } else if (/true|false/.test(match)) {
                cls = 'boolean';
            } else if (/null/.test(match)) {
                cls = 'null';
            }
            return '<span class="' + cls + '">' + match + '</span>';
        });
    }

    function replyok(obj) {
            console.log("replyok:" + JSON.stringify(obj));
            document.getElementById("output").innerHTML = "OK: "+ syntaxHighlight(obj);
    }

    function replyerr(obj) {
            console.log("replyerr:" + JSON.stringify(obj));
            document.getElementById("output").innerHTML = "ERROR: "+ syntaxHighlight(obj);
    }

    function gotevent(obj) {
            console.log("gotevent:" + JSON.stringify(obj));
            document.getElementById("outevt").innerHTML = (evtidx++) +": "+JSON.stringify(obj);
    }

    function send(message) {
            var api = document.getElementById("api").value;
            var verb = document.getElementById("verb").value;
            document.getElementById("question").innerHTML = "subscribe: "+api+"/"+verb + " (" + JSON.stringify(message) +")";
            ws.call(api+"/"+verb, {data:message}).then(replyok, replyerr);
    }

    // On button click from HTML page
    function callbinder(api, verb, query) {
            console.log ("subscribe api="+api+" verb="+verb+" query=" +query);
            var question = afb.urlws +"/" +api +"/" +verb + "?query=" + JSON.stringify(query);
            document.getElementById("question").innerHTML = syntaxHighlight(question);
            ws.call(api+"/"+verb, query).then(replyok, replyerr);
    }

    function init(elemid, api, verb, query) {

        var slimMaster = document.getElementById("slimamp-master");
        var ampMaster = document.getElementById("amplifier-master");

        // Update the current slider value (each time you drag the slider handle)
        slimMaster.oninput = function() {
            console.log ("slimamp-master: new value=%s", this.value);
            var params = {value: parseInt(this.value, 10)};
            callbinder("unicens-controller","slimamp_master_volume_set", params);
        }

        // Update the current slider value (each time you drag the slider handle)
        ampMaster.oninput = function() {
            console.log ("amplifier-master: new value=%s", this.value);
            var params = {value: parseInt(this.value, 10)};
            callbinder("unicens-controller","amplifier_master_volume_set", params);
        }

        function onopen() {
                console.log("onopen triggered");
                document.getElementById("main").style.visibility = "visible";
                document.getElementById("connected").innerHTML = "Binder WS Active";
                document.getElementById("connected").style.background  = "lightgreen";
                ws.onevent("*", gotevent);
        }

        function onabort() {
                console.log("onabort triggered");
                document.getElementById("main").style.visibility = "hidden";
                document.getElementById("connected").innerHTML = "Connected Closed";
                document.getElementById("connected").style.background  = "red";
        }

        ws = new afb.ws(onopen, onabort);
    }
